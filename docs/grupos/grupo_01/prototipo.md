<br/>

<div style="display: flex; justify-content: center; align-items:center;">
    <img src="https://dansousamelo.github.io/RQ_ISP/assets/PROTOTIPO.png" width="200" height="200" style="filter: brightness(0%);" />
</div>

<br/>

Protótipos de alta fidelidade são uma peça central no design e desenvolvimento de produtos, atuando como uma ponte entre as ideias conceituais e a realidade tangível. Eles são representações quase finais de um produto, que simulam a aparência, a sensação e, muitas vezes, a funcionalidade do design pretendido. Ao criar um protótipo de alta fidelidade, designers e desenvolvedores buscam oferecer uma experiência que se aproxime o máximo possível do produto final, permitindo testar interações, fluxos de usuário e interfaces gráficas de forma detalhada e realista.

## Histórico de versões

| Versão |    Data    |      Descrição       |                      Autor(es)                       | Revisor(es) |
| :----: | :--------: | :------------------: | :--------------------------------------------------: | :---------: |
| `1.0`  | 16/04/2024 | Criação do Documento do Protótipo | [Grupo 01](/grupos/grupo_01?id=integrantes-do-grupo) |      -      |



## Telas
<div align="justify">
&emsp;&emsp;Abaixo encontram-se o protótipo completo da aplicação desenvolvido no Figma, uma poderosa ferramenta de design colaborativo. O Figma permite que equipes trabalhem simultaneamente em projetos, facilitando a criação de interfaces de usuário intuitivas e agradáveis. Com sua interface amigável e recursos robustos, o Figma é amplamente utilizado por designers e equipes de desenvolvimento em todo o mundo. Explore o protótipo interativo abaixo para ter uma visão abrangente do trabalho realizado.
<br/>



<div style="display: flex; flex-direction: column; justify-content: center; align-items:center;">
   <iframe style="border: 1px solid rgba(0, 0, 0, 0.1);" width="800" height="450" src="https://www.figma.com/embed?embed_host=share&url=https%3A%2F%2Fwww.figma.com%2Ffile%2F1dpqTkNRHpdYYgkIXuLG39%2FReadFile%3Ftype%3Ddesign%26node-id%3D0%253A1%26mode%3Ddesign%26t%3DuehFYRMPloqxycdQ-1" allowfullscreen></iframe>
    <figure style="font-style: italic;">
    Aguarde o carregamento da visualização, ou se preferir vá direto ao link<a href="https://www.figma.com/file/1dpqTkNRHpdYYgkIXuLG39/ReadFile?type=design&mode=design&t=fuW2qKRRQfiGTXkE-1"><u>aqui</u></a>.
</figure>
</div>


## Referências

- Staiano, F. (2022). Designing and Prototyping Interfaces with Figma. Packt Publishing Ltd.