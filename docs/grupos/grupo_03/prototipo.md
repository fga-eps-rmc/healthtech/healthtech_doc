<br/>

<div style="display: flex; justify-content: center; align-items:center;">
    <img src="https://dansousamelo.github.io/RQ_ISP/assets/PROTOTIPO.png" width="200" height="200" style="filter: brightness(0%);" />
</div>

<br/>

Protótipos de alta fidelidade são uma peça central no design e desenvolvimento de produtos, atuando como uma ponte entre as ideias conceituais e a realidade tangível. Eles são representações quase finais de um produto, que simulam a aparência, a sensação e, muitas vezes, a funcionalidade do design pretendido. Ao criar um protótipo de alta fidelidade, designers e desenvolvedores buscam oferecer uma experiência que se aproxime o máximo possível do produto final, permitindo testar interações, fluxos de usuário e interfaces gráficas de forma detalhada e realista.

## Histórico de versões

| Versão |    Data    |      Descrição       |                      Autor(es)                       | Revisor(es) |
| :----: | :--------: | :------------------: | :--------------------------------------------------: | :---------: |
| `1.0`  | 17/04/2024 | Criação do Documento | Grupo 03 |      -      |



## Telas
<div align="justify">
&emsp;&emsp;Abaixo encontram-se o protótipo completo da aplicação desenvolvido no Figma, uma poderosa ferramenta de design colaborativo. O Figma permite que equipes trabalhem simultaneamente em projetos, facilitando a criação de interfaces de usuário intuitivas e agradáveis. Com sua interface amigável e recursos robustos, o Figma é amplamente utilizado por designers e equipes de desenvolvimento em todo o mundo. Explore o protótipo interativo abaixo para ter uma visão abrangente do trabalho realizado.
<br/>
<br/>

<iframe style="border: 1px solid rgba(0, 0, 0, 0.1);" width="800" height="450" src="https://www.figma.com/embed?embed_host=share&url=https%3A%2F%2Fwww.figma.com%2Ffile%2FyzZxMj2pjy9MVkmyyEBTXl%2FSPA-DataMED%3Ftype%3Ddesign%26node-id%3D0%253A1%26mode%3Ddesign%26t%3DsTxfvNBe7GGlVBhX-1" allowfullscreen></iframe>



## Referências

 * Staiano, F. (2022). Designing and Prototyping Interfaces with Figma. Packt Publishing Ltd.