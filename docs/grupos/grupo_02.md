<br/>

<div style="display: flex; flex-direction: column; justify-content: center; align-items:center;">
    <img src="https://dansousamelo.github.io/RQ_ISP/assets/ANALISE.png" width="200" height="200" style="filter: brightness(0%);"ss />
</div>

<br/>

Este documento tem como objetivo apresentar os integrantes do Grupo 02 e fornecer informações sobre o contexto em que estamos trabalhando.


## Histórico de versões

| Versão |    Data    |      Descrição       |                      Autor(es)                       | Revisor(es) |
| :----: | :--------: | :------------------: | :--------------------------------------------------: | :---------: |
| `1.0`  | 15/04/2024 | Criação do Documento | [Grupo 02](/grupos/grupo_02?id=integrantes-do-grupo) |      -      |

## Contexto do Grupo

O Grupo 21 está envolvido em um projeto para a disciplina EPS (Engenharia de Produto de Software). Este projeto tem como objetivo atender às necessidades do cliente Fernando Viana. O grupo será responsável por implementar as funcionalidades listadas no [Backlog do Produto referente ao Grupo 01](../grupos/grupo_02/backlog.md).

## Integrantes do Grupo

| Integrante                          | 
| ----------------------------------- |
| Antônio Rangel Chaves              |
| Kess Jhones Gomes Tavares |
| Victor Amaral Cerqueira   |
| Vítor Diniz Pagani Vieira Ribeiro     |

## Responsabilidades dos Integrantes

- **Antônio Rangel Chaves**: Desenvolvedor.
- **Kess Jhones Gomes Tavaress**: Desenvolvedor.
- **Victor Amaral Cerqueira**: Desenvolvedor.
- **Vítor Diniz Pagani Vieira Ribeiro**: Desenvolvedor.

## Metas e Objetivos

As metas e objetivos do Grupo 02 incluem realizar as atividades previstas do Backlog do Produto.

## Conclusão e Próximos Passos

Em resumo, o Grupo 02 é composto por 4 membros dedicados que estão trabalhando diligentemente para alcançar as metas estabelecidas para o projeto. Os próximos passos incluem realizar as sprints de desenvolvimento assim como garantir a qualidade das entregas.


