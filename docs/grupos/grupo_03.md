<br/>

<div style="display: flex; flex-direction: column; justify-content: center; align-items:center;">
    <img src="https://dansousamelo.github.io/RQ_ISP/assets/ANALISE.png" width="200" height="200" style="filter: brightness(0%);"ss />
</div>

<br/>

Este documento tem como objetivo apresentar os integrantes do Grupo 03 e fornecer informações sobre o contexto em que estamos trabalhando.


## Histórico de versões

| Versão |    Data    |      Descrição       |                      Autor(es)                       | Revisor(es) |
| :----: | :--------: | :------------------: | :--------------------------------------------------: | :---------: |
| `1.0`  | 15/04/2024 | Criação do Documento | Grupo 01 |      -      |

## Contexto do Grupo

O Grupo 03 está envolvido em um projeto para a disciplina EPS (Engenharia de Produto de Software). Este projeto tem como objetivo criar um microsserviço de gerencia de agendamentos, sejam médicos ou de outras especialidades. O grupo será responsável por implementar as funcionalidades listadas no [Backlog do Produto referente ao Grupo 03](../grupos/grupo_03/backlog.md).

## Integrantes do Grupo

| Integrantes                    |
| ------------------------------ |
| Carla Rocha Cangussú           |
| Felipe Correia Andrade         |
| João Paulo Coelho de Souza     |
| Mateus Brandão Teixeira        |

## Responsabilidades dos Integrantes

- **Carla Rocha Cangussú**: Desenvolvedor.
- **Felipe Correia Andrade**: Desenvolvedor.
- **João Paulo Coelho de Souza**: Desenvolvedor.
- **Mateus Brandão Teixeira**: Desenvolvedor.

## Metas e Objetivos

As metas e objetivos do Grupo 03 consistem em executar as atividades planejadas do Backlog do Produto.

## Conclusão e Próximos Passos

O Grupo 03 é formado por 4 membros comprometidos, empenhados em atingir as metas do projeto. Nossos próximos passos envolvem a execução das sprints de desenvolvimento e a garantia da qualidade das entregas.


