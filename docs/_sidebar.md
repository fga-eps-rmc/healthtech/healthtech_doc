* **Planejamento**
	* [Reuniões](reunioes/reunioes.md)
		* [Ata 30/03/2024](reunioes/ata_30_03_2024.md)
		* [Ata 06/04/2024](reunioes/ata_06_04_2024.md)
	* [Métricas e Indicadores](sprint/metricas.md)
		- [Sprint 0](sprint/sprint_0.md)

* **Artefatos**
	* [Lean Inception](reunioes/lean.md)
	* [Backlog Geral](reunioes/backlogGeral.md)

* **Entregas**
	* [Relatórios](relatorio/relatorios.md)
		* [Primeiro Relatório](relatorio/primeiro.md)

* **Contribuição**
	* [Como contribuir](guia_de_contribuicao/como_contribuir.md)
	<!-- * [Código de conduta](guia_de_contribuicao/codigo_de_conduta.md) -->
	* [Políticas de contribuições](guia_de_contribuicao/politicas_de_contribuicao_do_repositorio.md)

* **Grupos**
	* [Grupo 01](grupos/grupo_01)
		* [Plano de Testes e Qualidade](grupos/grupo_01/plano_de_teste_e_qualidade.md)
		* [Protótipo](grupos/grupo_01/prototipo.md)
		* [Backlog](grupos/grupo_01/backlog.md)
	* [Grupo 02](grupos/grupo_02)
		* [Protótipo](grupos/grupo_02/prototipo.md)
		* [Backlog](grupos/grupo_02/backlog.md)
	* [Grupo 03](grupos/grupo_03)
		* [Protótipo](grupos/grupo_03/prototipo.md)
		* [Backlog](grupos/grupo_03/backlog.md)