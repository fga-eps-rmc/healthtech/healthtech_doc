## Sobre

O DataMed é uma Aplicação Web cuja função é ajudar pacientes a compreenderem melhor informações sobre sua saúde de uma maneira intuitiva e prática e possibilitar a negociação de dados dos mesmos com profissionais da área da saúde, a fim de facilitar pesquisas médicas.

**GitLab do projeto dispinível em:** <a href="https://gitlab.com/fga-eps-rmc/healthtech"><u>DataMed - GitLab</u></a>

## Integrantes

Abaixo, nas Tabelas de 1 a 4 a seguir, estão os Grupos e seus integrantes.

### Grupo 01

| Membros                             | Função        |
| ----------------------------------- | ------------- |
| Arthur de Melo Garcia               | Desenvolvedor |
| Daniel de Sousa Oliveira Melo Veras | Desenvolvedor |
| Lucas Gabriel Sousa Camargo Paiva   | Desenvolvedor |
| Luiz Gustavo Dias Paes Pinheiro     | Desenvolvedor |

<div style="text-align: center">
<p> Tabela 1: Funções dos membros do grupo 01 (Fonte: autor, 2024). </p>
</div>

### Grupo 02

| Membros                           | Função        |
| --------------------------------- | ------------- |
| Antônio Rangel Chaves             | Desenvolvedor |
| Kess Jhones Gomes Tavares         | Desenvolvedor |
| Victor Amaral Cerqueira           | Desenvolvedor |
| Vítor Diniz Pagani Vieira Ribeiro | Desenvolvedor |

<div style="text-align: center">
<p> Tabela 2: Funções dos membros do grupo 02 (Fonte: autor, 2024). </p>
</div>

### Grupo 03

| Membros                    | Função        |
| -------------------------- | ------------- |
| Carla Rocha Cangussú       | Desenvolvedor |
| Felipe Correia Andrade     | Desenvolvedor |
| João Paulo Coelho de Souza | Desenvolvedor |
| Mateus Brandão Teixeira    | Desenvolvedor |

<div style="text-align: center">
<p> Tabela 3: Funções dos membros do grupo 03 (Fonte: autor, 2024). </p>
</div>

### Grupo 04

| Membros                     | Função        |
| --------------------------- | ------------- |
| Elias Yousef Santana Ali    | Desenvolvedor |
| Sávio Cunha de Carvalho     | Desenvolvedor |
| Thiago França Vale Oliveira | Desenvolvedor |

<div style="text-align: center">
<p> Tabela 4: Funções dos membros do grupo 04 (Fonte: autor, 2024). </p>
</div>