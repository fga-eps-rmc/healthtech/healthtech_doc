# Backlog do produto


## 1. Introdução

O backlog do produto é uma lista priorizada de itens que uma equipe de desenvolvimento planeja criar em um produto. Esses itens podem incluir funcionalidades, melhorias e correções, e são organizados com base em prioridades. O backlog é dinâmico e permite que as equipes ágeis se adaptem às mudanças nas necessidades do cliente e do mercado. O proprietário do produto é responsável por gerenciá-lo e garantir que os itens sejam desenvolvidos de acordo com as prioridades estabelecidas.


## 2. Backlog

### 2.1 Épicos

Épicos no backlog do produto representam grandes iniciativas ou metas de alto nível que são desmembradas em tarefas menores. Eles são usados para agrupar um conjunto de funcionalidades ou requisitos que, juntos, contribuem significativamente para o desenvolvimento de um produto. Esses épicos são uma ferramenta fundamental para gerenciar projetos complexos e manter o foco nas metas estratégicas. À medida que a equipe avança, os épicos são decompostos em histórias de usuário mais detalhadas e viáveis.

| ID | Épico | Grupo responsável |
|:--:|:-----:|:-----------------:|
| EP 01 | Gerência de Pacientes | Grupo 2 |
| EP 02 | Dados de Saúde | Grupo 1 e 2 |
| EP 03 | Agendamento de consultas | Grupo 3 |

### 2.2 Histórias de usuários

Histórias de usuário são uma técnica de documentação amplamente utilizada no desenvolvimento de software ágil. Elas são pequenas descrições de funcionalidades ou requisitos do ponto de vista do usuário. Cada história de usuário é uma narrativa concisa que descreve o que um usuário deseja alcançar ao interagir com um sistema ou software. Essas histórias são fundamentais para entender as necessidades do cliente e guiar o desenvolvimento de software centrado no usuário. Elas geralmente seguem um formato simples e são uma parte essencial do backlog do produto.

| Épico | ID | História de Usuário | Prioridade |
|:-----:|:--:|:-------------------:|:----------:|
| EP 01 | US 01 | Eu, como Usuário, desejo fazer login no DataMed | Must |
| EP 01 | US 02 | Eu, como Usuário, desejo fazer logout no DataMed | Must |
| EP 01 | US 03 | Eu, como Usuário, desejo me cadastrar no DataMed | Must |
| EP 01 | US 04 | Eu, como Usuário, desejo editar meus dados cadastrados | Must |
| EP 01 | US 05 | Eu, como Usuário, desejo excluir minha conta doo DataMed | Must |
| EP 01 | US 06 | Eu, como Usuário, desejo cadastrar meus dependentes no DataMed | Must |
| EP 01 | US 07 | Eu, como Usuário, desejo editar os dados dos meus dependentes no DataMed | Must |
| EP 01 | US 08 | Eu, como Usuário, desejo excluir as contas dos meus dependentes no DataMed | Must |
| EP 02 | US 09 | Eu, como Usuário, gostaria de visualizar um histórico detalhado de todos os exames realizados meus ou de meus dependentes. | Should |
| EP 02 | US 10 | Eu, como Usuário, gostaria de realizar upload de exames no formato PDF | Must |
| EP 02 | US 11 | Eu, como Usuário, gostaria que o sistema me guiasse cno processo de preencher os dados de saúde | Could |
| EP 02 | US 12 | Eu, como Usuário, gostaria que o sistema enviasse o cartão por E-mail | Could |
| EP 02 | US 13 | Eu, como Usuário, gostaria de que os dados contidos nos exames enviados por PDF fossem processados automaticamente | Should |
| EP 02 | US 14 | Eu, como Usuário, gostaria de preencher um formulário manualmente para o envio de exames | Must |
| EP 02 | US 15 | Eu, como Usuário, gostaria de gerar um cartão contendo resumo dos meus dados médicos ou de meus dependentes | Must |
| EP 02 | US 16 | Eu, como Usuário, gostaria de imprimir um cartão contendo resumo dos meus dados médicos ou de meus dependentes | Must |
| EP 03 | US 17 | Eu, como Administrador, gostaria de cadastrar novas instituições fornecendo nome, tipo, endereço e contato para expandir a rede de atendimento disponível no sistema. | Must |
| EP 03 | US 18 | Eu, como Administrador, gostaria de adicionar especialidades disponíveis em cada instituição durante o cadastro para que os pacientes possam escolher corretamente onde querem ser atendidos | Must |
| EP 03 | US 19 | Eu, como Administrador, gostaria de atualizar ou remover instituições já cadastradas para garantir que a informação no sistema esteja sempre atualizada e correta | Must |
| EP 03 | US 20 | Eu, como Administrador, gostaria de visualizar todas as informações de uma instituição, incluindo detalhes como especialidades oferecidas e médicos disponíveis, para gerenciar eficientemente as instituições. | Must |
| EP 03 | US 21 | Eu, como Administrador, gostaria de editar os detalhes das especialidades para corrigir erros ou atualizar informações. | Must |
| EP 03 | US 22 | Eu, como Administrador, gostaria de remover especialidades que não são mais oferecidas para evitar confusões no agendamento de consultas. | Must |
| EP 03 | US 23 | Eu, como Administrador, gostaria de cadastrar colaboradores no sistema, incluindo informações como nome, CPF e e-mail, para que eles possam ser associados a consultas e especialidades. | Must |
| EP 03 | US 24 | Eu, como Administrador, gostaria de associar colaboradores às instituições em que trabalham para que os pacientes possam agendar consultas corretamente. | Must |
| EP 03 | US 25 | Eu, como Administrador, gostaria de atualizar as informações dos colaboradores ou remover colaboradores do sistema quando necessário | Must |
| EP 03 | US 26 | Eu, como Usuário, gostaria de ter a opção de realizar um agendamento diretamente pelo sistema, facilitando a gestão do meu tempo e evitando deslocamentos desnecessários. | Must |
| EP 03 | US 27 | Eu, como Usuário, gostaria de ter a opção de cancelar ou fazer um reagendamento diretamente pelo sistema, facilitando a gestão do meu tempo e evitando deslocamentos desnecessários. | Must |
| EP 03 | US 28 | Eu, como Usuário, gostaria de poder modificar os detalhes de um agendamento em caso de necessidade, como alterações no horário ou no médico designado, para acomodar. | Should |
| EP 03 | US 29 | Eu, como Usuário, gostaria de poder confirmar ou recusar pedidos de reagendamento ou cancelamento para manter a ordem e eficiência dos agendamentos | Must |
| EP 03 | US 30 | Eu, como Usuário, gostaria de visualizar todas as minhas consultas agendadas, incluindo datas, horários, colaboradores e instituições, para que eu possa gerenciar meu tempo e compromissos de saúde eficientemente. | Must |

| Legenda MoSCoW | Descrição |
|:--------------:|:---------:|
| Must | Tarefas indispensáveis para a realização do projeto (Tenho que fazer) |
| Should | Tarefas importantes para a realização do projeto, mas não é fundamental (Deveria fazer) |
| Could| Tarefas interessantes e que poderiam ser feitas, mas não são tão importantes para o sucesso do produto (Poderia fazer) |
| Won't | Tarefas menos importantes e o time decide não fazer em um futuro próximo (Não vou fazer) |

## 3. Referências

> [1] Scrum Guides, 2020. Disponível em: https://scrumguides.org/scrum-guide.html

> [2] Método MoSCoW: framework para ajudar a priorizar tarefas. Disponível em: https://www.cursospm3.com.br/blog/metodo-moscow-framework-para-priorizar-tarefas/?gclid=CjwKCAjwscGjBhAXEiwAswQqNHSB3GKTPcK1SwagIS4eTFlQvnkGveIglZwapVnKx19CBrJekshxbBoCpMsQAvD_BwE

## 4. Histórico de versão

| Data | Versão | Descrição | Autor(es) |
|:----:|:------:|:---------:|:---------:|
|22/04/2024|1.0| Adição do documento | Victor Amaral e Antonio Rangel |