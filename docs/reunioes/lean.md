<br/>

<div style="display: flex; flex-direction: column; justify-content: center; align-items:center;">
    <img src="https://raw.githubusercontent.com/dansousamelo/RQ_ISP/docs/docs/assets/lean/LEAN-ICON.png" width="200" height="200" style="filter: brightness(0%);"ss />
</div>

<br/>

Neste documento, é apresentado de forma detalhada a condução do <i>Lean Inception</i>, documentando cada fase e os artefatos gerados ao longo do caminho.

## Histórico de versões

| Versão |    Data     |              Descrição              |  Autor(es)   |  Revisor(es)  |
| :----: | :---------: | :---------------------------------: | :----------: | :-----------: |
| `1.0`  | 05//04/2024 | Criando documento do Lean Inception | Daniel Veras | Lucas Gabriel |


## Introdução


A otimização da eficácia do Lean Inception é alcançada por meio de uma abordagem colaborativa com o cliente, destacando a importância da perspectiva do cliente na compreensão do produto pela equipe (Caroli, 2018). Validações contínuas foram realizadas durante o desenvolvimento do artefato para alinhar as visões da equipe e do proprietário do produto. Em vez das longas reuniões ao longo de uma semana preconizadas pelo Lean Inception, a equipe escolheu parear uma vez por semana por uma hora, demonstrando flexibilidade diante das dificuldades de conciliar horários. Essa abordagem mais acessível e eficiente foi adotada para garantir a participação ativa de todos os membros da equipe.

Para alcançar esse objetivo, utilizou-se uma combinação de tarefas síncronas e assíncronas. Atividades síncronas estimularam discussões e facilitaram a criação de determinados artefatos. Nas atividades assíncronas, os participantes preencheram os artefatos designados como tarefas a serem realizadas fora do horário das reuniões.

Um template foi empregado no Figma para facilitar a execução das etapas do Lean Inception, proporcionando uma abordagem estruturada e eficiente no desenvolvimento de projetos. Essa metodologia auxilia as equipes a definirem os aspectos mais críticos do projeto, incluindo o escopo, os objetivos principais e as prioridades, garantindo um alinhamento claro e uma visão compartilhada entre todos os envolvidos. A utilização de ferramentas visuais, como o Figma, otimiza a colaboração e a comunicação dentro da equipe, tornando o processo de Lean Inception mais dinâmico e interativo.


<br/>
<div style="display: flex; flex-direction: column; justify-content: center; align-items:center;">
    <iframe src="https://embed.figma.com/file/1358057445573703417/hf_embed?community_viewer=true&embed_host=fastma&fuid=775860797041235232&hub_file_id=1358057445573703417&kind=file&viewer=1" width="800" height="600" frameborder="0" allowfullscreen></iframe>
    <figure style="font-style: italic;">
    Aguarde o carregamento da visualização, ou se preferir vá direto ao link<a href="https://www.figma.com/file/OmRP8xHyarx2wv53mJpfA3/DataMed?type=whiteboard&node-id=104-1251"><u>aqui</u></a>.
</figure>

</div>
<br/>





## Referências

- Caroli, P. (2018). Lean Inception: Como alinhar pessoas e construir o produto certo. Caroli.






